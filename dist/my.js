// Teacher
var myTable;
$(document).ready(function () {

    showall();

    function showall() {
        myTable = $('#dataTeacher').DataTable({
            "ajax": '/class/ts/showTeacher',
            "columnDefs": [{
                "className": 'select-checkbox',
                "targets": [0,6,7],
                "orderable": false
            }],
            "select": {
                style: 'os',
                selector: 'td:first-child'
            },
            "order": [[1, 'asc']],
            "language": {
                "paginate": {
                    "next": ">",
                    "previous": "<"
                },
                "info": "第_START_~ _END_ " + "&nbsp;" + "&nbsp;" + " 共_TOTAL_項",
                "sLengthMenu": "顯示 _MENU_ 項",
                "sSearch": "搜尋:",


            }
        });
    }

    var client = mqtt.connect('ws://118.167.102.144:8883')
    // var rfid = $('rfid').val();
    client.on('connect', function () {
        client.subscribe('where/teacher')
    });
    client.on('message', function (topic, message) {
        if (topic == 'where/teacher') {
            $('[name = "Trfid"]').val(message.toString('utf8'));
        }
        if (topic == 'iot/devicestatus') {

            $('[name = "Trfid"]').val(message.toString('utf8'));
        }
    });

    $('#btn_save').on('click', function () {
        var name = $('#name').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var phone = $('#phone').val();
        var office = $('#office').val();
        var photo = $('#photo').val();
        var status = $('#Status').is(':checked') ? 1 : 0;
        $.ajax({
            type: 'post',
            url: '/class/ts/addTeacher',
            dataType: 'json',
            data: {
                tname: name,
                email: email,
                password: password,
                phone: phone,
                office: office,
                status: status,
                photo: photo
            },
            success: function (data) {
                $('[name = "name"]').val("");
                $('[name = "email"]').val("");
                $('[name = "password"]').val("");
                $('[name = "phone"]').val("");
                $('[name = "office"]').val("");
                $('[name = "photo"]').val("");
                // $('[name = "Trfid"]').val("");
                $('#addModal').modal('hide');
                myTable.ajax.reload(null, false);
                myTable.page('last').draw('page');
            }
        });
        return false;
    });

    //取得資料
    $('#showdata').on('click', '.update', function () {
        var id = $(this).data('id');
        var name = $(this).data('name');
        var email = $(this).data('email');
        var password = $(this).data('password');
        var phone = $(this).data('phone');
        var office = $(this).data('office');

        $('#editModal').modal('show');
        $('[name = "editID"]').val(id);
        $('[name = "edit_name"]').val(name);
        $('[name = "edit_email"]').val(email);
        $('[name = "edit_password"]').val(password);
        $('[name = "edit_phone"]').val(phone);
        $('[name = "edit_office"]').val(office);
    });


    //更新置資料庫
    $('#btnUpdate').on('click', function () {
        var id = $('#editID').val();
        var name = $('#edit_name').val();
        var email = $('#edit_email').val();
        var password = $('#edit_password').val();
        var phone = $('#edit_phone').val();
        var office = $('#edit_office').val();

        $.ajax({
            type: 'post',
            url: '/class/ts/updateTeacher',
            dataType: 'json',
            data: {id: id, tname: name, email: email, password: password, phone: phone, office: office},
            success: function (data) {
                $('[name ="editID"]').val("");
                $('[name = "edit_name"]').val("");
                $('[name = "edit_email"]').val("");
                $('[name = "edit_password"]').val("");
                $('[name = "edit_phone"]').val("");
                $('[name = "edit_office"]').val("");
                $('#editModal').modal('hide');
                myTable.ajax.reload(null, false);

            }
        });
        return false;
    });

    //刪除

    $('#showdata').on('click', '.delete', function () {
        var id = $(this).data('id');
        $('#deleteModal').modal('show');
        // alert(id);
        $('[name = "deleteID"]').val(id);
    });

    $('#btn_delete').on('click', function () {
        var id = $('#deleteID').val();
        $.ajax({
            type: 'post',
            url: '/class/ts/deleteTeacher',
            dataType: 'json',
            data: {id: id},
            success: function (data) {
                $('[name = "deleteID"]').val("");
                $('#deleteModal').modal('hide');
                myTable.ajax.reload(null, false);
            }
        });
    });

    $('#showdata').on('click', '.stop', function () {
        var id = $(this).data('id');
        // alert(id);
        if (confirm('是否起用?')) {
            $.ajax({
                type: 'post',
                url: '/class/ts/active',
                dataType: 'json',
                data: {id: id, status: status},
                success: function (data) {
                    myTable.ajax.reload(null, false);
                }
            });
        }
    });

    $('#showdata').on('click', '.start', function () {
        var id = $(this).data('id');
        // alert(id);
        if (confirm('是否停用?')) {
            $.ajax({
                type: 'post',
                url: '/class/ts/inactive',
                dataType: 'json',
                data: {id: id, status: status},
                success: function (data) {
                    myTable.ajax.reload(null, false);
                }
            });
        }
    });

    $('#ChkboxT').on('click', function () {
        if ($(this).is(':checked', true)) {
            $('.data-checkT').prop('checked', true);
        } else {
            $('.data-checkT').prop('checked', false);
        }
    });

    var list_id = [];
    $('#chkDeleteT').on('click', function () {

        $(' .data-checkT:checked').each(function () {
            list_id.push(this.value);
        });

        if (list_id.length > 0) {
            if (confirm('確定刪除' + list_id + '資料?')) {
                // var id = list_id.join(",");
                // list_id.push(this.value);
                $.ajax({
                    type: 'post',
                    url: '/class/ts/list_deleteT',
                    dataType: 'json',
                    data: {id: list_id},
                    success: function (data) {
                        myTable.ajax.reload(null, false);
                    }
                });
            }
        } else {
            alert('沒有選擇');
        }

    });

});


//Student

var studentTable;
$(document).ready(function () {

    showStudent();

    function showStudent() {
        studentTable = $('#dataStudent').DataTable({
            "ajax": '/class/ts/showStudent',
            "columnDefs": [{
                "className": 'select-checkbox',
                "targets": [0,6,7],
                "orderable": false
            }],
            "select": {
                style: 'os',
                selector: 'td:first-child'
            },
            "order": [[1, 'asc']],
            "language": {
                "paginate": {
                    "next": ">",
                    "previous": "<"
                },
                "info": "第_START_~ _END_ " + "&nbsp;" + "&nbsp;" + " 共_TOTAL_項",
                "sLengthMenu": "顯示 _MENU_ 項",
                "sSearch": "搜尋:"

            }
        });
    }

    //新增

    var client = mqtt.connect('ws://118.167.102.144:8883');
    var rfid = $('#rfid').val();
    // var erfid = $('#e_rfid').val();
    client.on('connect', function () {
        client.subscribe('where/teacher')
    });
    client.on('message', function (topic, message) {
        if (topic == 'where/teacher') {
            $('[name = "rfid"]').val(message.toString('utf8'));
            $('[name = "e_rfid"]').val(message.toString('utf8'));
            // alert(message.toString('utf8'));
        }
    });

    $('#btn_s').on('click', function () {
        var sname = $('#s_name').val();
        var email = $('#s_email').val();
        var password = $('#s_password').val();
        var phone = $('#s_phone').val();
        var rfid = $('#rfid').val();
        var status = $('#sStatus').is(':checked') ? 1 : 0;
        $.ajax({
            type: 'post',
            url: '/class/ts/addStudent',
            dataType: 'json',
            data: {sname: sname, email: email, password: password, phone: phone, rfid: rfid, status: status},
            success: function (data) {
                $('[name = "s_name"]').val("");
                $('[name = "s_email"]').val("");
                $('[name = "s_password"]').val("");
                $('[name = "s_phone"]').val("");
                $('[name = "sStatus"]').val("");
                // $('[name = "rfid"]').val(message.toString('utf8'));
                $('#addModal').modal('hide');
                studentTable.ajax.reload(null, false);
                studentTable.page('last').draw('page');
            }
        });
        return false;
    });

    //取得資料
    $('#showStudent').on('click', '.update', function () {
        var id = $(this).data('id');
        var name = $(this).data('name');
        var email = $(this).data('email');
        var password = $(this).data('password');
        var phone = $(this).data('phone');
        var rfid = $(this).data('rfid');
        $('#editModal').modal('show');
        $('[name = "e_ID"]').val(id);
        $('[name = "e_name"]').val(name);
        $('[name = "e_email"]').val(email);
        $('[name = "e_password"]').val(password);
        $('[name = "e_phone"]').val(phone);
        $('[name = "e_rfid"]').val(rfid);
    });


    //更新置資料庫
    $('#btnU').on('click', function () {
        var id = $('#e_ID').val();
        var name = $('#e_name').val();
        var email = $('#e_email').val();
        var password = $('#e_password').val();
        var phone = $('#e_phone').val();
        var rfid = $('#e_rfid').val();
        // var status = $('#eStatus').val();

        $.ajax({
            type: 'post',
            url: '/class/ts/updateStudent',
            dataType: 'json',
            data: {id: id, sname: name, email: email, password: password, phone: phone,rfid : rfid},
            success: function (data) {
                $('[name ="e_ID"]').val("");
                $('[name = "e_name"]').val("");
                $('[name = "e_email"]').val("");
                $('[name = "e_password"]').val("");
                $('[name = "e_phone"]').val("");
                $('[name = "e_rfid"]').val("");
                // $('[name = "eStatus"]').val("");
                $('#editModal').modal('hide');
                studentTable.ajax.reload(null, false);

            }
        });
        return false;
    });

    //刪除

    $('#showStudent').on('click', '.delete', function () {
        var id = $(this).data('id');
        $('#deleteModal').modal('show');
        $('[name = "d_ID"]').val(id);
    });

    $('#btn_d').on('click', function () {
        var id = $('#d_ID').val();
        $.ajax({
            type: 'post',
            url: '/class/ts/deleteStudent',
            dataType: 'json',
            data: {id: id},
            success: function (data) {
                $('[name = "d_ID"]').val("");
                $('#deleteModal').modal('hide');
                studentTable.ajax.reload(null, false);
            }
        });
    });

    $('#showStudent').on('click', '.stop', function () {
        var id = $(this).data('id');
        // var status = $(this).is(':checked')?1:0;
        // alert(id);
        if (confirm('是否起用?')) {
            $.ajax({
                type: 'post',
                url: '/class/ts/sActive',
                dataType: 'json',
                data: {id: id, status: status},
                success: function (data) {
                    // $('[name = "sStatus"]').val("");
                    studentTable.ajax.reload(null, false);
                }
            });
        }
    });

    $('#showStudent').on('click', '.start', function () {
        var id = $(this).data('id');
        // var status = $(this).is(':checked')?1:0;
        // alert(id);
        if (confirm('是否停用?')) {
            $.ajax({
                type: 'post',
                url: '/class/ts/sInactive',
                dataType: 'json',
                data: {id: id, status: status},
                success: function (data) {
                    // $('[name = "sStatus"]').val("");
                    studentTable.ajax.reload(null, false);
                }
            });
        }
    });


    $('#ChkBox').on('click', function () {
        if ($(this).is(':checked', true)) {
            $('.data-checkS').prop('checked', true);
        } else {
            $('.data-checkS').prop('checked', false);
        }
    });

    var list_idS = [];
    $('#chkDelete').on('click', function () {

        $(' .data-checkS:checked').each(function () {
            list_idS.push(this.value);
        });

        if (list_idS.length > 0) {
            if (confirm('確定刪除' + list_idS + '資料?')) {
                // var id = list_id.join(",");
                // list_id.push(this.value);
                $.ajax({
                    type: 'post',
                    url: '/class/ts/list_delete',
                    dataType: 'json',
                    data: {id: list_idS},
                    success: function (data) {
                        studentTable.ajax.reload(null, false);
                    }
                });
            }
        } else {
            alert('沒有選擇');
        }

    });

});



