<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php

$CI =& get_instance();
$CI->load->library('layout');

function menuClassOpen($str)
{
    $CI =& get_instance();
    $result = '';
    if ($CI->router->class == 'admin') {
        if ($str == 'tree') $result = 'active';
        if ($str == 'open') $result = 'menu-open';
        if ($str == 'style') $result = 'display : block';
    }
    return $result;
}

function menuClassActive($checkRoute)
{
    $CI =& get_instance();
    $result = '';
    $routePath = '/class/' . $CI->router->class . '/' . $CI->router->method;
    if ($routePath == $checkRoute) {
        $result = 'class = "active" ';
    }
    return $result;
}

?>

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="zh-tw">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>後台</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/dist/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/dist/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <!-- DataTables -->


    <!-- Theme style -->
    <link rel="stylesheet" href="/dist/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="/dist/dist/css/skins/skin-blue.min.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
    <script src="/dist/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="/dist/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- DataTables -->

    <!--    <script src="--><?php //echo base_url()?><!--dist/jquery-3.1.1.min.js"></script>-->
    <!-- SlimScroll -->

    <!-- FastClick -->

    <!-- AdminLTE App -->
    <script src="/dist/dist/js/adminlte.min.js"></script>
    <script src="/dist/dist/js/demo.js"></script>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="/class/admin/dashboard" class="logo ">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>LT</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">Admin</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->

            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->

                    <!-- /.messages-menu -->

                    <!-- Notifications Menu -->

                    <!-- Tasks Menu -->

                    <!-- User Account Menu -->

                    <!-- Control Sidebar Toggle Button -->

                    <li class="pull-right">
                        <a href="/class/admin/logout" class="btn btn-flat ">登出</a>
                    </li>

                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">


            <!-- Sidebar Menu -->
            <ul class="sidebar-menu  " data-widget="tree" data-api="tree" data-accordion="0">
                <!-- Optionally, you can add icons to the links -->
                <li><a href="/class/mqtt/box"><i class="fa fa-wrench"></i><span>裝置維護</span></a></li>
                <li><a href="/class/admin/curriculum"><i class="fa fa-table"></i><span>課表維護</span></a></li>
                <li><a href="/class/admin/messages"><i class="fa fa-commenting"></i><span>留言管理</span></a></li>
                <li><a href="/class/admin/chat"><i class="fa fa-comments-o"></i><span>即時聊天管理</span></a></li>

                <li class="treeview <?php echo menuClassOpen('tree'); ?> "
                    style="<?php echo menuClassOpen('style'); ?>">
                    <a href="#"><i class="fa fa-calendar"></i> <span>行事曆訊息管理</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu <?php echo menuClassOpen('open'); ?>">
                        <li <?php echo menuClassActive('/class/admin/teacher'); ?> >
                            <a href="/class/admin/calendar"><i class="fa fa-file-text"></i><span>老師行事曆行程</span></a>
                        </li>
                        <li <?php echo menuClassActive('/class/admin/student'); ?> >
                            <a href="/class/admin/calendar_send"><i class="fa fa-file-text-o "></i><span>學生預約行程</span></a>
                        </li>
                    </ul>
                </li>





                <li class="header" style="font-size: medium ">用戶管理</li>

                <li class="treeview <?php echo menuClassOpen('tree'); ?> "
                    style="<?php echo menuClassOpen('style'); ?>">
                    <a href="#"><i class="fa fa-user"></i> <span>帳號管理</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu <?php echo menuClassOpen('open'); ?>">
                        <li <?php echo menuClassActive('/class/admin/teacher'); ?> >
                            <a href="/class/ts/teacher"><i class="fa fa-file-text"></i><span>老師</span></a>
                        </li>
                        <li <?php echo menuClassActive('/class/admin/student'); ?> >
                            <a href="/class/ts/student"><i class="fa fa-file-text-o "></i><span>學生</span></a>
                        </li>
                    </ul>
                </li>
                <li><a href="/class/admin/rfid"><i class="fa  fa-wifi"></i> <span>RFID</span></a></li>
<!--                <li><a href="/class/admin/third_part"><i class="fa fa-google-plus-square"></i><span>第三方帳號</span></a></li>-->


            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <?php echo $body_content; ?>
        <!-- Content Header (Page header) -->
        <!--------------------------
          | Your Page Content Here |
          -------------------------->
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <!-- Default to the left -->
        <strong>Copyright &copy; <a href="https://adminlte.io/">Almsaeed Studio</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->


<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->



</body>
</html>