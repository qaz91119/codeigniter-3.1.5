<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        留言管理
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-gear"></i>主控台</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">留言</h3>

                    <div class="box-tools">
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="box" class="table table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>學生ID</th>
                            <th>學生姓名</th>
                            <th>留言</th>
                            <th>老師ID</th>
                            <th>老師姓名</th>
                        </tr>
                        </thead>
                        <?php foreach ($show as $value){?>
                            <tbody>
                            <tr>
                                <td><?php echo "$value->id";?></td>
                                <td><?php echo "$value->sid";?></td>
                                <td><?php echo "$value->name";?></td>
                                <td><?php echo "$value->message";?></td>
                                <td><?php echo "$value->tid";?></td>
                                <td><?php echo $value->name;?></td>
                            </tr>
                            </tbody>
                        <?php }?>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
