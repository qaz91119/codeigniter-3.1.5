<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" href="/dist/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<script src="/dist/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/dist/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        即時聊天管理
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-gear"></i>主控台</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">留言</h3>
                    <div class="box-tools">
                        <button type="button" id="chkDelete" class="btn btn-success btn-sm"> 刪除</button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="chat" class="table table-hover">
                        <thead>
                        <tr>
                            <th><input type="checkbox" id="chkBox"></th>
                            <th>ID</th>
                            <th>學生姓名</th>
                            <th>老師姓名</th>
                            <th>老師內容</th>
                            <th>學生內容</th>
                            <th>時間</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
<script>
    var chatTable;
    $(document).ready(function () {
        show();
        function show() {
            chatTable = $('#chat').DataTable({
//                'paging': false,
//                'lengthChange': false,
//                'searching': true,
//                'ordering': false,
//                'info': false,
//                'autoWidth': false,
                "ajax": '/class/admin/showChat',
                "columnDefs": [{
                    "className": 'select-checkbox',
                    "targets": 0,
                    "orderable": false
                }],
                "select": {
                    style: 'os',
                    selector: 'td:first-child'
                },
                "order": [[1, 'asc']],
                "language": {
                    "paginate": {
                        "next": ">",
                        "previous": "<"
                    },
                    "info": "第_START_~ _END_ " + "&nbsp;" + "&nbsp;" + " 共_TOTAL_項",
                    "sLengthMenu": "顯示 _MENU_ 項",
                    "sSearch": "搜尋:"


                }
            });
        }

        $('#chkBox').on('click', function () {
            if ($(this).is(':checked', true)) {
                $('.data-checkCHAT').prop('checked', true);
            } else {
                $('.data-checkCHAT').prop('checked', false);
            }
        });

        var list_id = [];
        $('#chkDelete').on('click', function () {

            $(' .data-checkCHAT:checked').each(function () {
                list_id.push(this.value);
            });

            if (list_id.length > 0) {
                if (confirm('確定刪除' + list_id + '資料?')) {

                    $.ajax({
                        type: 'post',
                        url: '/class/admin/list_delete_chat',
                        dataType: 'json',
                        data: {id: list_id},
                        success: function (data) {
                            chatTable.ajax.reload(null, false);
                        }
                    });
                }
            } else {
                alert('沒有選擇');
            }

        });
    });
</script>