<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vistors</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/dist/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/dist/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/dist/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/dist/dist/css/AdminLTE.min.css">
    <!-- iCheck -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <![endif]-->

    <style>
        .loginbox {
            margin: 180px auto;
            width: 400px;
            position: relative;
            border-radius: 15px;
            background: #ffffff;
        }

        body {
            background-color: rgb(209, 209, 209);
        }
    </style>

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box ">

    <!-- /.login-logo -->
    <div class="login-box-body loginbox">
        <div class="login-logo">
            <h1>Login</h1>
        </div>

        <form action="/class/admin/login" method="post">
            <div class="form-group has-feedback">
                <input type="email" name="email" id="email" class="form-control" placeholder="Account">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>

            </div>

            <?php if ($this->session->flashdata("Error")) ; ?>
            <p><?= $this->session->flashdata("Error") ?></p>

<!--            <div class="row">-->


                <!-- /.col -->

                <div class="box-footer">
                    <button type="reset" class="btn btn-default pull-left">清除</button>
                    <button type="submit" class="btn btn-info pull-right btn-flat " id="sub">登入</button>
                    <div class="text-center">
                        <a href="<?php echo $loginURL;?>" class="btn btn-default btn-social btn-google">
                            <i class="fa fa-google-plus"></i> Sign in with Google
                        </a>
                    </div>
                </div>

                <!-- /.col -->
<!--            </div>-->

        </form>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="/dist/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/dist/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
