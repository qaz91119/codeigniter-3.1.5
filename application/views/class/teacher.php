<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="/dist/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<script src="/dist/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/dist/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="http://unpkg.com/mqtt@2.13.1/dist/mqtt.min.js"></script>
<script src="https://unpkg.com/mqtt/dist/mqtt.min.js"></script>
<script src="/dist/my.js"></script>
<section class="content-header">
    <h1>帳號管理</h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>帳號管理</a></li>

        <li class="active">使用者資訊</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">教師資訊</h3>

                    <div class="box-tools">
                        <button type="button" id="chkDeleteT" class="btn btn-success btn-sm">刪除</button>
                        <button data-toggle="modal" data-target="#addModal" class="btn btn-primary btn-sm">新增</button>
                    </div>

                </div>

                <div class="box-body ">
                    <table id="dataTeacher" class="table table-hover">
                        <thead>
                        <tr>
                            <th><input type="checkbox" id="ChkboxT"></th>
                            <th>NO.</th>
                            <th>頭像</th>
                            <th>姓名</th>
                            <th>信箱</th>
                            <th>電話</th>
                            <th>辦公室</th>
                            <th>建立日期</th>
                            <th>狀態</th>
                            <th>編輯</th>
                        </tr>
                        </thead>
                        <tbody id="showdata">

                        </tbody>
                    </table>
                </div>

                <!--新增-->
                <div id="addModal" class="modal fade " tabindex="-1" role="dialog">
                    <div class="modal-dialog  " role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title">新增教師資訊</h3>
                            </div>
                            <div class="modal-body">
                                <form id="addForm" action="" method="post" class="form-horizontal">
                                    <input type="hidden" class="form-control" name="id">
                                    <div class="box-body">

                                        <div class="form-group">
                                            <label for="name">姓名</label>
                                            <input type="text" class="form-control" name="name" id="name"
                                                   placeholder="使用者名稱">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">信箱</label>
                                            <input type="text" class="form-control" name="email" id="email"
                                                   placeholder="信箱">
                                        </div>
                                        <div class="form-group">
                                            <label for="password">密碼</label>
                                            <input type="password" class="form-control" name="password" id="password"
                                                   placeholder="密碼">
                                        </div>
                                        <div class="form-group">
                                            <label for="phone">電話</label>
                                            <input type="text" class="form-control" name="phone" id="phone"
                                                   placeholder="電話">
                                        </div>
                                        <div class="form-group">
                                            <label for="office">辦公室</label>
                                            <input type="text" class="form-control" name="office" id="office"
                                                   placeholder="辦公室">
                                        </div>
                                        <div class="form-group">
                                            <label for="photo">請選擇照片</label>
                                            <input type="file" id="photo" name="photo">
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="Status" id="Status"> 啟用
                                            </label>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="btn_save" class="btn btn-primary">確定</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->


                <!--修改 -->
                <div id="editModal" class="modal fade " tabindex="-1" role="dialog">
                    <div class="modal-dialog  " role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title">修改教師資訊</h3>
                            </div>
                            <div class="modal-body">
                                <form id="editmyForm" action="" method="post" class="form-horizontal">

                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="name">No.</label>
                                            <input type="text" name="editID" id="editID" class="form-control"
                                                   placeholder="No." readonly>
                                        </div>

                                        <div class="form-group">
                                            <label for="name">姓名</label>
                                            <input type="text" class="form-control" name="edit_name" id="edit_name"
                                                   placeholder="使用者名稱">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">信箱</label>
                                            <input type="text" class="form-control" name="edit_email" id="edit_email"
                                                   placeholder="信箱">
                                        </div>
                                        <div class="form-group">
                                            <label for="password">密碼</label>
                                            <input type="password" class="form-control" name="edit_password"
                                                   id="edit_password"
                                                   placeholder="密碼">
                                        </div>
                                        <div class="form-group">
                                            <label for="phone">電話</label>
                                            <input type="text" class="form-control" name="edit_phone" id="edit_phone"
                                                   placeholder="電話">
                                        </div>
                                        <div class="form-group">
                                            <label for="office">辦公室</label>
                                            <input type="text" class="form-control" name="edit_office" id="edit_office"
                                                   placeholder="辦公室">
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" id="btnUpdate" class="btn btn-primary">確定</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <!--刪除-->
                <div id="deleteModal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">刪除教師資訊</h4>
                            </div>
                            <div class="modal-body">
                                <h4> 是否刪除?</h4>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="deleteID" id="deleteID" class="form-control">
                                <button type="submit" id="btn_delete" class="btn btn-danger">確認</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

            </div>
        </div>
    </div>
</section>

