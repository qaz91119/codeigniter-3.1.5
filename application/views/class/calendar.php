<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link rel="stylesheet" href="/dist/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<script src="/dist/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/dist/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="http://unpkg.com/mqtt@2.13.1/dist/mqtt.min.js"></script>
<script src="https://unpkg.com/mqtt/dist/mqtt.min.js"></script>

<section class="content-header">
    <h1>老師行事曆行程</h1>
    <ol class="breadcrumb">
        <li><a href="/class/admin/calendar"><i class="fa fa-dashboard"></i>帳號管理</a></li>
        <li class="active">行事曆訊息管理</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">行事曆</h3>
                    <div class="box-tools">
                        <button type="button" id="chkDelete" class="btn btn-success btn-sm"> 刪除</button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="chat" class="table table-hover">
                        <thead>
                        <tr>
                            <th><input type="checkbox" id="chkBox"></th>
                            <th>訊息編號</th>
                            <th>老師</th>
                            <th>行事曆行程</th>
                            <th>行程開始時間</th>
                            <th>行程結束時間</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>

<script>
    var chatTable;
    $(document).ready(function () {
        show();
        function show() {
            chatTable = $('#chat').DataTable({
//                'paging': false,
//                'lengthChange': false,
//                'searching': true,
//                'ordering': false,
//                'info': false,
//                'autoWidth': false,
                "ajax": '/class/admin/showtitle',
                "columnDefs": [{
                    "className": 'select-checkbox',
                    "targets": 0,
                    "orderable": false
                }],
                "select": {
                    style: 'os',
                    selector: 'td:first-child'
                },
                "order": [[1, 'asc']],
                "language": {
                    "paginate": {
                        "next": ">",
                        "previous": "<"
                    },
                    "info": "第_START_~ _END_ " + "&nbsp;" + "&nbsp;" + " 共_TOTAL_項",
                    "sLengthMenu": "顯示 _MENU_ 項",
                    "sSearch": "搜尋:"


                }
            });
        }

        $('#chkBox').on('click', function () {
            if ($(this).is(':checked', true)) {
                $('.data-checkTitle').prop('checked', true);
            } else {
                $('.data-checkTitle').prop('checked', false);
            }
        });

        var list_id = [];
        $('#chkDelete').on('click', function () {

            $(' .data-checkTitle:checked').each(function () {
                list_id.push(this.value);
            });

            if (list_id.length > 0) {
                if (confirm('確定刪除' + list_id + '資料?')) {

                    $.ajax({
                        type: 'post',
                        url: '/class/admin/list_delete_title',
                        dataType: 'json',
                        data: {id: list_id},
                        success: function (data) {
                            chatTable.ajax.reload(null, false);
                        }
                    });
                }
            } else {
                alert('沒有選擇');
            }

        });
    });
</script>
