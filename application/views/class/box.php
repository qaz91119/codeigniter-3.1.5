<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" href="/dist/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<script src="/dist/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/dist/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="http://unpkg.com/mqtt@2.13.1/dist/mqtt.min.js"></script>
<script src="https://unpkg.com/mqtt/dist/mqtt.min.js"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        WebDuino
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-gear"></i>主控台</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">裝置狀態</h3>

                    <div class="box-tools">
                        <button type="button" id="chkDelete" class="btn btn-success btn-sm"> 刪除</button>
                        <button data-toggle="modal" data-target="#addModal" class="btn btn-primary btn-sm">新增</button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="box" class="table table-hover">
                        <thead>
                        <tr>
                            <th><input type="checkbox" id="chkBox"></th>
                            <th>ID</th>
                            <th>老師</th>
                            <th>辦公室</th>

                            <!--                            <th>裝置編號</th>-->
                            <th>裝置名稱</th>
                            <th>機器狀態</th>
                            <th>老師狀態</th>
                        </tr>
                        </thead>
                        <tbody id="showBox">

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>



    <!--新增-->
    <div id="addModal" class="modal fade " tabindex="-1" role="dialog">
        <div class="modal-dialog  " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">新增教師資訊</h3>
                </div>
                <div class="modal-body">
                    <form id="addForm" action="" method="post" class="form-horizontal">
                        <input type="hidden" class="form-control" name="id">
                        <div class="box-body">

                            <div class="form-group">
                                <label for="tid">老師名稱</label>
                                <input type="tid" class="form-control" name="tid" id="tid"
                                       placeholder="老師名稱">
                            </div>
                            <div class="form-group">
                                <label for="office">辦公室</label>
                                <input type="office" class="form-control" name="office" id="office"
                                       placeholder="辦公室">
                            </div>
                            <div class="form-group">
                                <label for="name">裝置名稱</label>
                                <input type="name" class="form-control" name="name" id="name"
                                       placeholder="裝置名稱">
                            </div>


                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn_save" class="btn btn-primary">確定</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-




</section>
<!-- /.content -->
<script>
    var boxTable;
    $(document).ready(function () {
        show();
        function show() {
            boxTable = $('#box').DataTable({
                'paging': false,
                'lengthChange': false,
                'searching': true,
                'ordering': false,
                'info': false,
                'autoWidth': false,
                "ajax": '/class/mqtt/showbox',
                "columnDefs": [{
                    "className": 'select-checkbox',
                    "targets": 0,
                    "orderable": false
                }],
                "select": {
                    style: 'os',
                    selector: 'td:first-child'
                },
                "order": [[1, 'asc']],
                "language": {
                    "sSearch": "搜尋:"
                }
            });
//            setTimeout(function() {
//                boxTable.ajax.reload();
//            }, 3000);
//            setInterval( function () {
//                location.reload(); // user paging is not reset on reload
//            }, 3000 );

        }
//
//        var client = mqtt.connect('ws://140.131.7.50:8883');
////        var test = 'device ready!';
////        var status = $('status').val();
////        var rfid = $('rfid').val();
//        client.on('connect', function () {
//            client.subscribe('where/teacher');
//            client.subscribe('where');
////            client.subscribe('iot/deviceStatus');
//        });
//        client.on('message', function (topic, message) {
////            if (topic == 'iot/deviceStatus') {
////                if (message.toString('utf8') == '01') {}
////            }
//
//            if (topic == 'where') {
////                if (message.toString('utf8') == test) {
//                    alert(message.toString('utf8'));
////                }
//            }
//        });

//        $('#showBox').on('click', '.stop', function () {
//            var id = $(this).data('id');
//            // alert(id);
//            if (confirm('是否起用?')) {
//                $.ajax({
//                    type: 'post',
//                    url: '/class/mqtt/active',
//                    dataType: 'json',
//                    data: {id: id, status: status},
//                    success: function (data) {
//                        myTable.ajax.reload(null, false);
//                    }
//                });
//            }
//        });
//
//        $('#showBox').on('click', '.start', function () {
//            var id = $(this).data('id');
//            // alert(id);
//            if (confirm('是否停用?')) {
//                $.ajax({
//                    type: 'post',
//                    url: '/class/mqtt/inactive',
//                    dataType: 'json',
//                    data: {id: id, status: status},
//                    success: function (data) {
//                        myTable.ajax.reload(null, false);
//                    }
//                });
//            }
//        });
        $('#chkBox').on('click', function () {
            if ($(this).is(':checked', true)) {
                $('.data-checkS').prop('checked', true);
            } else {
                $('.data-checkS').prop('checked', false);
            }
        });

        var list_id = [];
        $('#chkDelete').on('click', function () {

            $(' .data-checkS:checked').each(function () {
                list_id.push(this.value);
            });

            if (list_id.length > 0) {
                if (confirm('確定刪除' + list_id + '資料?')) {

                    $.ajax({
                        type: 'post',
                        url: '/class/mqtt/list_box',
                        dataType: 'json',
                        data: {id: list_id},
                        success: function (data) {
                            boxTable.ajax.reload(null, false);
                        }
                    });
                }
            } else {
                alert('沒有選擇');
            }

        });
        $('#btn_save').on('click', function () {
            var tid = $('#tid').val();
            var office = $('#office').val();
            var name = $('#name').val();
            $.ajax({
                type: 'post',
                url: '/class/mqtt/addwebduino',
                dataType: 'json',
                data: {
                    tid: tid,
                    office: office,
                    name: name,
                },
                success: function (data) {
                    $('[tid = "tid"]').val("");
                    $('[office = "office"]').val("");
                    $('[name = "name"]').val("");
                    // $('[name = "Trfid"]').val("");
                    $('#addModal').modal('hide');
                    boxTable.ajax.reload(null, false);
                    boxTable.page('last').draw('page');
                }
            });
            return false;
        });
    });
</script>