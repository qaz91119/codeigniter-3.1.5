<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" href="/dist/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<script src="/dist/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/dist/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        RFID
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-gear"></i>主控台</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">紀錄</h3>
                    <div class="box-tools">
                        <button type="button" id="chkDelete" class="btn btn-success btn-sm"> 刪除</button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="rfid" class="table table-hover">
                        <thead>
                        <tr>
                            <th><input type="checkbox" id="chkBox"></th>
                            <th>ID</th>
                            <th>學生姓名</th>
                            <th>老師姓名</th>

                            <th>刷卡時間</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
<script>
    var rfidTable;
    $(document).ready(function () {
        show();
        function show() {
            rfidTable = $('#rfid').DataTable({
//                'paging': false,
//                'lengthChange': false,
//                'searching': true,
//                'ordering': false,
//                'info': false,
//                'autoWidth': false,
                "ajax": '/class/admin/showRfid',
                "columnDefs": [{
                    "className": 'select-checkbox',
                    "targets": 0,
                    "orderable": false
                }],
                "select": {
                    style: 'os',
                    selector: 'td:first-child'
                },
                "order": [[1, 'asc']],
                "language": {
                    "paginate": {
                        "next": ">",
                        "previous": "<"
                    },
                    "info": "第_START_~ _END_ " + "&nbsp;" + "&nbsp;" + " 共_TOTAL_項",
                    "sLengthMenu": "顯示 _MENU_ 項",
                    "sSearch": "搜尋:"


                }
            });
        }

        $('#chkBox').on('click', function () {
            if ($(this).is(':checked', true)) {
                $('.data-checkRFID').prop('checked', true);
            } else {
                $('.data-checkRFID').prop('checked', false);
            }
        });

        var list_id = [];
        $('#chkDelete').on('click', function () {

//            $(' .data-checkRFID:checked').each(function () {
//                list_id.push(this.value);
//            });
            $(' .data-checkRFID:checked').map(function () {
                list_id.push(this.value);
            });

            if (list_id.length > 0) {
                if (confirm('確定刪除' + list_id + '資料?')) {

                    $.ajax({
                        type: 'post',
                        url: '/class/admin/list_delete_rfid',
                        dataType: 'json',
                        data: {id: list_id},
                        success: function (data) {
                            rfidTable.ajax.reload(null, false);
                        }
                    });
                }
            } else {
                alert('沒有選擇');
            }

        });
    });
</script>