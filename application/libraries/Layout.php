<?php
/**
 * CodeIgnighter layout support library
 *  with Twig like inheritance blocks
 *
 * v 1.0
 *
 *
 * @author Constantin Bosneaga
 * @email  constantin@bosneaga.com
 * @url    http://a32.me/
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Layout {
    private $obj;
    private $layout_view;
    private $layout_view_mng;
    private $title = '';
    private $keywords = '';
    private $description = '';
    private $author = '';
    private $css_list = array(), $js_list = array();
    private $block_list, $block_new, $block_replace = false;

    private $subDirectoryName = '';

    function Layout() {
        $this->obj =& get_instance();
        $this->layout_view_mng = "";
        // Grab layout from called controller
        if (isset($this->obj->layout_view_mng)) $this->layout_view_mng = $this->obj->layout_view_mng;
    }

    public function getSubDirName() {
        $result = '';
        $CI =& get_instance();
        $CI->load->helper('url');
        $matchs = array();
        $currentUri = uri_string();
        $isMatch = preg_match("/\/?(\w+)\/\w+/", $currentUri, $matchs);

        if ($isMatch) {
            return $matchs[1];
        }

        return $result;
    }

    function view($view, $data = null, $return = false) {
        // Render template
        $data['body_content'] = $this->obj->load->view($view, $data, true);
        $data['head_title'] = $this->title;
        $data['head_meta_keywords'] = $this->keywords;
        $data['head_meta_description'] = $this->description;
        $data['head_meta_author'] = $this->author;

        // Render resources
        $data['head_js'] = '';
        foreach ($this->js_list as $v)
            $data['head_js'] .= sprintf('<script src="%s"></script>', $v);

        $data['head_css'] = '';
        foreach ($this->css_list as $v)
            $data['head_css'] .= sprintf('<link rel="stylesheet" type="text/css"  href="%s" />', $v);

        // Render template
        $this->block_replace = true;

        $viewPath = $this->getSubDirName();

        //進後台的話,載入後台的Layout
        if ($viewPath == 'class'){
            $output = $this->obj->load->view($this->layout_view_mng, $data, $return);
        }

        return $output;
    }

    /**
     * Set page title
     *
     * @param $title
     */
    function title($title) {
        $this->title = $title;
    }
    function keywords($keywords) {
        $this->keywords = $keywords;
    }
    function description($description) {
        $this->description = $description;
    }
    function author($author) {
        $this->author = $author;
    }
    /**
     * Adds Javascript resource to current page
     * @param $item
     */
    function js($item) {
        $this->js_list[] = $item;
    }

    /**
     * Adds CSS resource to current page
     * @param $item
     */
    function css($item) {
        $this->css_list[] = $item;
    }

    /**
     * Twig like template inheritance
     *
     * @param string $name
     */
    function block($name = '') {
        if ($name != '') {
            $this->block_new = $name;
            ob_start();
        } else {
            if ($this->block_replace) {
                // If block was overriden in template, replace it in layout
                if (!empty($this->block_list[$this->block_new])) {
                    ob_end_clean();
                    echo $this->block_list[$this->block_new];
                }
            } else {
                $this->block_list[$this->block_new] = ob_get_clean();
            }
        }
    }

}