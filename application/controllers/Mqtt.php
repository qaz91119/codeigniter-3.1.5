<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mqtt extends CI_Controller
{

    public function __construct()
    {
        header('Content-type : application/json');
        header('Access-Control-Allow-Origin : *');
        header("Access-Control-Allow-Methods:GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        $this->load->library('Matt', array(
            'address' => 'test.mosquitto.org',
            'port' => 1883,
            'clientid' => '7dc1a724-af28-4568-b912-4f837949846c'
        ));
    }

    public function read($userid=''){
        if(!empty($userid)){

        }
    }

    private function mqttpublish($jsonStr){
        if($this->mqtt->connect()){
            $this->mqtt->publish("iot/webduino",$jsonStr,0);
            $this->mqtt->close();
        }
    }

}

