<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mqtt extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('mqtt_model');
    }

    public function box()
    {
        $this->layout->view('class/box');
    }

    public function showBox()
    {
        $getBox = $this->mqtt_model->box();
        $data = array();
        foreach ($getBox as $value) {
            $row = array();
            $row[] = '<input type="checkbox" class ="data-checkS"   value="' . $value->id . '" >';
            $row[] = $value->id;
            $row[] = $value->tname;
            $row[] = $value->office;
            $row[] = $value->name;
            $status = $value->status;
            if ($status) {
                $row[] = '<a href="javascript:void(0);" class="btn btn-xs btn-success start" data-id = "' . $value->id . '" 
                                                                    data-status = "' . $value->status . '">啟動中</a>';
            } else {
                $row[] = '<a href="javascript:void(0);" class="btn btn-xs btn-danger stop" data-id = "' . $value->id . '"
                                                                    data-status = "' . $value->status . '">停用中</a>';
            }
//            $row[] = $value->wid;
            $led = $value->led;
            if ($led == 0 ) {
                $row[] = '<a href="javascript:void(0);" class="btn btn-xs btn-success green" data-id = "' . $value->id . '" 
                                                                    data-led = "' . $value->led . '">有空</a>';
            } else if($led == 1) {
                $row[] = '<a href="javascript:void(0);" class="btn btn-xs btn-danger red" data-id = "' . $value->id . '"
                                                                    data-led = "' . $value->led . '">忙碌中</a>';
            }
            else{
                $row[] = '<a href="javascript:void(0);" class="btn btn-xs btn-warning yellow" data-id = "' . $value->id . '"
                                                                    data-status = "' . $value->status . '">馬上回來</a>';

            }

            $data[] = $row;
        }
        $output = array("data" => $data);

        echo json_encode($output);
    }


    public function active(){
        $data = $this->mqtt_model->active();
        echo json_encode($data);
    }

    public function inactive(){
        $data = $this->mqtt_model->inactive();
        echo json_encode($data);
    }
    public function green(){
        $data = $this->mqtt_model->green();
        echo json_encode($data);
    }


    public function red(){
        $data = $this->mqtt_model->red();
        echo json_encode($data);
    }

    public function yellow(){
        $data = $this->mqtt_model->yellow();
        echo json_encode($data);
    }

    public function list_box()
    {

        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $data = $this->mqtt_model->list_box($id);
        }
        echo json_encode($data);
    }



    public function addwebduino()
    {
        $data = $this->mqtt_model->addwebduino();
        echo json_encode($data);
    }





}