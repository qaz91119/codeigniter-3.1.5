<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TS extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('admin_model');

    }

    public function teacher()
    {
        $this->layout->view('class/teacher');
    }

    public function showTeacher()
    {
        $getdata = $this->admin_model->showTeacher();
        $data = array();
        foreach ($getdata as $value) {
            $row = array();
            $row[] = '<input type="checkbox" class ="data-checkT" value="' . $value->id . '" >';
            $row[] = $value->id;
            $row[] = $value->photo;
            $row[] = $value->tname;
            $row[] = $value->email;
            $row[] = $value->phone;
            $row[] = $value->office;
            $row[] = $value->create_at;
//                        $row[] = $value->status;
            $status = $value->status;
            if ($status) {
                $row[] = '<a href="javascript:void(0);" class="btn btn-xs btn-success start" data-id = "' . $value->id . '" 
                                                                    data-status = "' . $value->status . '">啟用</a>';
            } else {
                $row[] = '<a href="javascript:void(0);" class="btn btn-xs btn-warning stop" data-id = "' . $value->id . '"
                                                                    data-status = "' . $value->status . '">停用</a>';
            }
            $row[] = '<a href="javascript:void(0);"  class="btn btn-info btn-xs  update "
                                data-id ="' . $value->id . '" data-name ="' . $value->tname . '" data-email ="' . $value->email . '"
                                data-password ="' . $value->password . '" data-phone ="' . $value->phone . '" data-office ="' . $value->office . '"
                      >修改</a>
                      <a href="javascript:void(0);"   class="btn btn-danger btn-xs delete" data-id = "' . $value->id . '">刪除</a>';
            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );

        echo json_encode($output);
    }




    public function active()
    {
        $data = $this->admin_model->active();
        echo json_encode($data);
    }

    public function inactive()
    {
        $data = $this->admin_model->inactive();
        echo json_encode($data);
    }

    public function addTeacher()
    {

        $data = $this->admin_model->addTeacher();
        echo json_encode($data);
    }

    public function editTeacher()
    {
        $data = $this->admin_model->editTeacher();
        echo json_encode($data);
    }


    public function updateTeacher()
    {
        $data = $this->admin_model->updateTeacher();
        echo json_encode($data);
    }

    public function deleteTeacher()
    {
        $data = $this->admin_model->deleteTeacher();
        echo json_encode($data);
    }

    public function list_deleteT()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $data = $this->admin_model->list_deleteT($id);
        }
        echo json_encode($data);
    }


    public function student()
    {
        $this->layout->view('class/student');
    }

    public function showStudent()
    {
        $getdata = $this->admin_model->showStudent();
        $data = array();
        foreach ($getdata as $value) {
            $row = array();
            $row[] = '<input type="checkbox" class ="data-checkS"   value="' . $value->id . '" >';
            $row[] = $value->id;
            $row[] = $value->sname;
            $row[] = $value->email;
            $row[] = $value->phone;
            $row[] = $value->rfid;
            $row[] = $value->w_status;
            $row[] = $value->create_at;
            $status = $value->status;
            if ($status) {
                $row[] = '<a href="javascript:void(0);" class="btn btn-xs btn-success start" data-id = "' . $value->id . '" 
                                                                    data-status = "' . $value->status . '">啟用</a>';
            } else {
                $row[] = '<a href="javascript:void(0);" class="btn btn-xs btn-warning stop" data-id = "' . $value->id . '"
                                                                    data-status = "' . $value->status . '">停用</a>';
            }
            $row[] = '<a href="javascript:void(0);"  class="btn btn-info btn-xs  update "
                                data-id ="' . $value->id . '" data-name ="' . $value->sname . '" data-email ="' . $value->email . '"
                                data-password ="' . $value->password . '" data-phone ="' . $value->phone . '">修改</a>
                      <a href="javascript:void(0);"   class="btn btn-danger btn-xs delete" data-id = "' . $value->id . '">刪除</a>';
            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );

        echo json_encode($output);
    }

    public function sActive()
    {
        $data = $this->admin_model->sActive();
        echo json_encode($data);
    }

    public function sInactive()
    {
        $data = $this->admin_model->sInactive();
        echo json_encode($data);
    }

    public function addStudent()
    {
        $data = $this->admin_model->addStudent();
        echo json_encode($data);
    }

    public function editStudent()
    {
        $data = $this->admin_model->editStudent();
        echo json_encode($data);
    }


    public function updateStudent()
    {
        $data = $this->admin_model->updateStudent();
        echo json_encode($data);
    }

    public function deleteStudent()
    {
        $data = $this->admin_model->deleteStudent();
        echo json_encode($data);
    }

    public function list_delete()
    {
//        $data = $this->admin_model->list_delete();
//        echo json_encode($data);
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $data = $this->admin_model->list_delete($id);
        }
        echo json_encode($data);
    }

}