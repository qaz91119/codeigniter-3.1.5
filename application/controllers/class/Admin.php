<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
//        $this->load->library('form_validation');
        $this->load->library('session');
//        $this->load->library('pagination');
        $this->load->library('googleplus');
        $this->load->model('admin_model');

    }

    public function login()
    {
        if ($this->input->method() == 'post') {
            $ipt = $this->input->post();
            $status = 1;

            if (!$ipt['email'] || !$ipt['password']) {
                $this->session->set_flashdata('Error', '請輸入帳號密碼'
                );
                redirect('/class/admin/login');
            }
            $chkLogin = $this->admin_model->checkLogin($ipt['email'], $ipt['password'], $status);
            $login = $this->admin_model->login($ipt['email'], $ipt['password']);

            if ($chkLogin == TRUE) {
                redirect('/class/admin/dashboard');
            } else {
                if ($login == FALSE) {
                    $this->session->set_flashdata('Error', '請重新輸入'
                    );
                    redirect('/class/admin/login');
                }
                $this->session->set_flashdata('Error', '帳號已停用'
                );
                redirect('/class/admin/login');
            }


        }
        if (isset($_GET['code'])) {
            $this->googleplus->getAuthenticate();


            $this->session->set_userdata('login', true);
            $this->session->set_userdata('userProfile', $this->googleplus->getUserInfo());
            redirect('/class/admin/dashboard');
        }
        $data['loginURL'] = $this->googleplus->loginURL();
        $this->load->view('class/login', $data);
    }

    //google測試取得
//    public function profile(){
//        if($this->session->userdata('login') == true)
//        {
//            $data['profileData'] = $this->session->userdata('userProfile');
//            $this->load->view('profile',$data);
//        }
//        else
//        {
//            redirect('');
//        }
//    }

    public function logout()
    {
        $this->session->sess_destroy();
        $this->googleplus->revokeToken('login'); //清除google login
        redirect('/class/admin/login/');
    }


    public function check()
    {


    }

    public function third_part()
    {
        $this->layout->view('class/third_part');
    }

    public function rfid()
    {
        $this->layout->view('class/rfid');
    }

    public function showRfid(){
        $getdata = $this->admin_model->showRfid();
        $data = array();
        foreach ($getdata as $value){
            $row = array();
            $row[] = '<input type="checkbox" class ="data-checkRFID"   value="' . $value->id . '" >';
            $row[] = $value->id;
            $row[] = $value->sname;
            $row[] = $value->tname;
            $row[] = $value->date;
            $data[] = $row;
        }
        $output = array("data" => $data);
        echo json_encode($output);
    }



    public function list_delete_rfid()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $data = $this->admin_model->list_delete_rfid($id);
        }
        echo json_encode($data);
    }

    public function dashboard()
    {
//        if ($this->session->userdata('check') == true) {
//            $data['profileData'] = $this->session->userdata('userProfile');
        $this->layout->view('class/dashboard');
//        }
//        else {
//            $this->layout->view('class/dashboard');
//        }
//        $this->layout->view('class/dashboard');
    }

    public function messages()
    {
//        $data['show'] = $this->admin_model->leaveMessage();
//
//        $this->layout->view('class/
        $this->layout->view('class/messages');
    }



    public function chat(){
        $this->layout->view('class/chat');
    }

    public function showChat(){
        $getdata = $this->admin_model->showChat();
        $data = array();
        foreach ($getdata as $value){
            $row = array();
            $row[] = '<input type="checkbox" class ="data-checkCHAT"   value="' . $value->id . '" >';
            $row[] = $value->id;
            $row[] = $value->sname;
            $row[] = $value->tname;
            $row[] = $value->pid;
            $row[] = $value->chat;
            $row[] = $value->create_at;
            $data[] =$row;
        }

        $output = array("data" => $data);

        echo json_encode($output);
    }

    public function list_delete_chat()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $data = $this->admin_model->list_delete_chat($id);
        }
        echo json_encode($data);
    }
    public function calendar(){
        $this->layout->view('class/calendar');
    }

    public function showtitle(){
        $getdata = $this->admin_model->showtitle();
        $data = array();
        foreach ($getdata as $value){
            $row = array();
            $row[] = '<input type="checkbox" class ="data-checkTitle"   value="' . $value->id . '" >';
            $row[] = $value->id;
            $row[] = $value->tname;
            $row[] = $value->title;
            $row[] = $value->start_event;
            $row[] = $value->end_event;
            $data[] =$row;
        }

        $output = array("data" => $data);

        echo json_encode($output);
    }

    public function list_delete_title()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $data = $this->admin_model->list_delete_title($id);
        }
        echo json_encode($data);
    }

    public function calendar_send(){
        $this->layout->view('class/calendar_send');
    }

    public function showsend(){
        $getdata = $this->admin_model->showsend();
        $data = array();
        foreach ($getdata as $value){
            $row = array();
            $row[] = '<input type="checkbox" class ="data-checkTitle"   value="' . $value->id . '" >';
            $row[] = $value->id;
//            $row[] = $value->sid;
//            $row[] = $value->tid;
            $row[] = $value->sname;
            $row[] = $value->tname;
            $row[] = $value->send;
            $row[] = $value->status;
            $row[] = $value->start_event;
            $row[] = $value->end_event;
            $data[] =$row;
        }

        $output = array("data" => $data);

        echo json_encode($output);
    }


    public function curriculum(){
        $this->layout->view('class/curriculum');
    }

    public function showleave(){
        $getdata = $this->admin_model->showleave();
        $data = array();
        foreach ($getdata as $value){
            $row = array();
            $row[] = '<input type="checkbox" class ="data-checkLeave"   value="' . $value->id . '" >';
            $row[] = $value->id;
            $row[] = $value->sname;
            $row[] = $value->tname;
            $row[] = $value->message;
            $row[] = $value->date;
            $data[] =$row;
        }

        $output = array("data" => $data);
        echo json_encode($output);
    }

    public function list_delete_leave()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $data = $this->admin_model->list_delete_leave($id);
        }
        echo json_encode($data);
    }
}