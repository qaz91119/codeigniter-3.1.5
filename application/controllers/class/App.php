<?php defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller
{
//    private $ipt;
//    private $method;

    public function __construct()
    {
        header('Content-type: application/json');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        header("Access-Control-Allow-Headers: userToken, Content-Type");


        parent::__construct();
        $this->load->model('app_model');


    }

    public function test()
    {

//        $output = $this->app_model->login();
        if ($this->input->method() == 'post') {
            $postData = $this->input->post();

            if ($postData['firstname'] == 'xxx') {
                $output = array(
                    'id' => 1,
                    'name' => 'andersen'
                );
                echo json_encode($output);
            }
        }

    }

    public function jwt()
    {
        if ($this->input->method() == 'post') {
            $ipt = $this->input->post();
            if ($ipt['email'] || $ipt['password']) {
                $token = jwt_helper::create($ipt['email']);
                $returnJson = json_encode(array(
                    'token' => $token
                ));

                echo $returnJson;
            }
        }
    }

    public function getSampleData()
    {
        if ($this->input->method() == 'get') {

            // 從header把token資訊取出來
            $headers = $this->input->request_headers();

            // 驗證token
            if (isset($headers['userToken'])) {
                if (jwt_helper::validate($headers['userToken'])) {
                    echo json_encode(array(
                        'result' => '驗證成功，返回資料'
                    ));
                } else {
                    echo json_encode(array(
                        'result' => '驗證失敗'
                    ));
                }
            }
        }
    }

    public function login()
    {

        if ($this->input->method() == 'post') {
            $ipt = $this->input->post();
            $status = 1;
            $getLogin = $this->app_model->getLogin($ipt['email'], $ipt['password'], $status);
            $login = $this->app_model->login($ipt['email'], $ipt['password']);

            if ($getLogin == TRUE) {

                foreach ($getLogin as $value) {
                    $data = array(
                        'id' => "$value->id",
                        'name' => "$value->sname",
                        'email' => "$value->email",
                        'w_status' => $value->w_status
                    );
                }
                echo json_encode($data);
            } else {
                if ($login == FALSE) {
                    echo 1;
                }
                echo 2;
            }

        }


    }

    public function teacher_login()
    {

        if ($this->input->method() == 'post') {
            $ipt = $this->input->post();
            $status = 1;
            $getLogin = $this->app_model->get_teacher_Login($ipt['email'], $ipt['password'], $status);
            $login = $this->app_model->teacher_Login($ipt['email'], $ipt['password']);
            if ($getLogin == TRUE) {
                foreach ($getLogin as $value) {
                    $data = array(
                        'id' => "$value->id",
                        'name' => "$value->tname",
                        'email' => "$value->email",
                        'password' => "$value->password",
                        'phone' => "$value->phone",
                        'office' => "$value->office",
                    );
                }
                echo json_encode($data);
            } else {
                if ($login == FALSE) {
                    echo 1;
                }
                echo 2;
            }
        }
    }


    public
    function showTeacher()
    {

        $getdata = $this->app_model->getTeacher();
        $data = array();
        foreach ($getdata as $value) {
            $row = array();
            $row["id"] = $value->id;
            $row["name"] = $value->tname;
            $row["email"] = $value->email;
            $row['photo'] = $value->photo;
            $row['password'] = $value->password;
            $row['phone'] = $value->phone;
            $row['office'] = $value->office;
            $data[] = $row;
        }
        $output = json_encode(array(
                'data' => $data,
            )
        );

        echo $output;

    }

    public function settingTeacher()
    {
        $data = $this->app_model->settingTeacher();
        echo json_encode($data);
    }




//    public function getTeacher(){
//        $data = $this->app_model->getTeacher();
//        echo json_encode($data);
//    }

    public function getStudent()
    {
        $getdata = $this->app_model->getStudent();
        $data = array();
        foreach ($getdata as $value) {
            $row = array(
//                'id' => $value->id,
//                'name' => $value->sname,
//                'w_status' => $value->w_status,
            );
            $row['id'] = $value->id;
            $row['name'] = $value->sname;
            $row['email'] = $value->email;
            $row['password'] = $value->password;
            $row['phone'] = $value->phone;
            $row['rfid'] = $value->rfid;
            $row['w_status'] = $value->w_status;
            $data[] = $row;

        }
        echo json_encode(array("data" => $data));
    }

    public
    function aaa()
    {
        $getdata = $this->admin_model->aaa();
        $data = array();
        foreach ($getdata as $value) {
            $row = array();
            $row[] = $value->name;
            $data[] = $row;
        }
        $output = array("data" => $data);

        echo json_encode($output);
    }


    public
    function getChat()
    {
        $getdata = $this->app_model->getChat();
        $data = array();
        foreach ($getdata as $value) {
            $row = array();
            $row["sid"] = $value->sid;
            $row["sname"] = $value->sname;
            $row["tid"] = $value->tid;
            $row["tname"] = $value->tname;
            $row["chat"] = $value->chat;
            $row['create_at'] = $value->create_at;
            $row["pid"] = $value->pid;
            $data[] = $row;
        }
//        echo json_encode($data);
        $output = json_encode(array(
                'data' => $data,
            )
        );

        echo $output;
    }

    public
    function insertChat()
    {
        $data = $this->app_model->insertChat();
        echo json_encode($data);
    }


    public
    function startStatus()
    {
        $data = $this->app_model->startStatus();
        echo json_encode($data);
    }

    public
function stopStatus()
{
    $data = $this->app_model->stopStatus();
    echo json_encode($data);
}



    public
    function indexStop()
    {
        $data = $this->app_model->indexStop();
        echo json_encode($data);
    }


    public
    function showChatStatus()
    {
        $data = $this->app_model->showChatStatus();
        echo json_encode($data);
    }


    public
    function insertLeave()
    {
        $data = $this->app_model->insertLeaveMessage();
        echo json_encode($data);
    }

    public function showLeave()
    {
        $getdata = $this->app_model->showLeave();
        $data = array();
        foreach ($getdata as $value) {
            $row = array();
//            $row["id"] = '<input type="checkbox" class ="data-checkRFID"   value="' . $value->id . '" >';
            $row["id"] = $value->id;
            $row["tid"] = $value->tid;
            $row["sid"] = $value->sname;
            $row["message"] = $value->message;
            $row["date"] = $value->date;
            $data[] = $row;
        }

        $output = array("data" => $data);
        echo json_encode($output);
    }

    public function list_Leave()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $data = $this->app_model->list_Leave($id);
        }
        echo json_encode($data);
    }

    public function showSLeave()
    {
        $getdata = $this->app_model->showSLeave();
        $data = array();
        foreach ($getdata as $value) {
            $row = array();
//            $row["id"] = '<input type="checkbox" class ="data-checkRFID"   value="' . $value->id . '" >';
            $row["id"] = $value->id;
            $row["sid"] = $value->sid;
            $row["tid"] = $value->tname;
            $row["message"] = $value->message;
            $row["date"] = $value->date;
            $data[] = $row;
        }
        $output = array("data" => $data);
        echo json_encode($output);
    }

    public function list_SLeave()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $data = $this->app_model->list_SLeave($id);
        }
        echo json_encode($data);
    }


    public
    function loadEvents()
    {
        $data = $this->app_model->loadEvents();
            echo json_encode($data);

    }

    public
    function insertEvents()
    {
        $data = $this->app_model->insertEvents();
        echo json_encode($data);
    }

    public
    function updateEvents()
    {
        $data = $this->app_model->updateEvents();
        echo json_encode($data);
    }


    public
    function deleteEvents()
    {
        $data = $this->app_model->deleteEvents();
        echo json_encode($data);
    }


    public
    function showSend()
    {
        $getdata = $this->app_model->showSend();
        $data = array();
        foreach ($getdata as $value) {
            $row = array();
            $row[] = '<input type="checkbox" class ="data-checkRFID"   value="' . $value->id . '" >';
//            $row[] = $value->id;
            $row[] = $value->sname;
            $row[] = $value->send;
            $row[] = $value->status;
            $row[] = $value->start_event;
            $row[] = $value->end_event;
            $row[] = '<a href="javascript:void(0);" class="btn btn-xs btn-success start" data-id = "' . $value->id . '" 
                                                                    data-status = "' . $value->title . '">確認</a>';
            $row[] = '<a href="javascript:void(0);"   class="btn btn-danger btn-xs delete" data-id = "' . $value->id . '">取消</a>';
            $data[] = $row;
        }

        $output = array("data" => $data);

        echo json_encode($output);
    }

    public
    function insertSend()
    {
        $data = $this->app_model->insertSend();
        echo json_encode($data);
    }

    public
    function loadSend()
    {
        $data = $this->app_model->loadSend();
        if ($data == TRUE) {
            echo json_encode($data);
        }

    }

    public
    function list_Send()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $data = $this->app_model->list_send($id);
        }
        echo json_encode($data);
    }


    public
    function list_mesend()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $data = $this->app_model->list_mesend($id);
        }
        echo json_encode($data);
    }


    public
    function check_send()
    {
        $data = $this->app_model->check_send();
        echo json_encode($data);
    }

    public
    function cancel_send()
    {
        $data = $this->app_model->cancel_send();
        echo json_encode($data);
    }


    public
    function meSend()
    {
        $getdata = $this->app_model->meSend();
        $data = array();
        foreach ($getdata as $value) {
            $row = array();
            $row[] = '<input type="checkbox" class ="data-checkRFID"   value="' . $value->id . '" >';
            $row[] = $value->tname;
            $row[] = $value->status;
            $row[] = $value->send;
            $row[] = $value->start_event;
            $row[] = $value->end_event;

            $row[] = '<a href="javascript:void(0);"   class="btn btn-danger btn-xs delete" data-id = "' . $value->id . '">取消</a>';

            $data[] = $row;
        }

        $output = array("data" => $data);

        echo json_encode($output);
    }

    public
    function deleteSend()
    {
        $data = $this->app_model->deleteSend();
        echo json_encode($data);
    }

    public function showLed()
    {

    }
//    public function settingTeacher()
//    {
//        $data = $this->app_model->settingTeacher();
//        echo json_encode($data);
//    }

    public function showrfid()
    {
        $getdata = $this->app_model->showrfid();
        $data = array();
        foreach ($getdata as $value) {
            $row = array();
//            $row[] = '<input type="checkbox" class ="data-checkRFID"   value="' . $value->id . '" >';
            $row["id"] = $value->id;
            $row["tid"] = $value->tid;
            $row["tname"] = $value->tname;
            $row["sid"] = $value->sname;
            $row["date"] = $value->date;
            $data[] = $row;
        }
//        echo json_encode(array("data" => $data));


        $output = array("data" => $data);
        echo json_encode($output);
    }

    public
    function list_rfid()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $data = $this->app_model->list_rfid($id);
        }
        echo json_encode($data);
    }
}

