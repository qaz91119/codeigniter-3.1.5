<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mqtt_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function box(){

//        $query = $this->db->get('webduino');
//        return $query->result();
        $sql = 'SELECT w.id,w.tid,w.office,w.name,w.led,w.status,t.tname from webduino w,teacher t where w.tid = t.id';
        $query = $this->db->query($sql);
        return $query->result();

    }

    public function active(){
        $id = $_REQUEST['id'];
        $status = 1;
//        $data = array('status' => $status);
        $this->db->set('status',$status);
        $this->db->where('id', $id);
        $result = $this->db->update('box');
        return $result;

        //        $id = $_REQUEST['id'];
//        $id = $this->input->get('id');
//        $status = 1;
//        $data = array('status' => $status);
//        $this->db->where('id', $id);
//        $result = $this->db->update('box', $data);
//        return $result;
    }

    public function inactive()
    {
        $id = $_REQUEST['id'];
        $status = 0;
        $data = array('status' => $status);
        $this->db->where('id', $id);
        $result = $this->db->update('box', $data);
        return $result;
    }
    public function green()
    {
        $id = $_REQUEST['id'];
        $led = '0';
        $data = array('led' => $led);
        $this->db->where('id', $id);
        $result = $this->db->update('webduino', $data);
        return $result;

    }


    public function red()
    {
        $id = $_REQUEST['id'];
        $led ='1' ;
        $data = array('led' => $led);
        $this->db->where('id', $id);
        $result = $this->db->update('webduino', $data);
        return $result;
    }

    public function yellow()
    {
        $id = $_REQUEST['id'];
        $led = '2';
        $data = array('led' => $led);
        $this->db->where('id', $id);
        $result = $this->db->update('webduino', $data);
        return $result;
    }


    public function list_box($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('webduino');
    }





    public function addwebduino()
    {
        $data = array(
            'tid' => $this->input->post('tid'),
            'office' => $this->input->post('office'),
            'name' => $this->input->post('name'),
        );
        $result = $this->db->insert('webduino', $data);
        return $result;

    }







}