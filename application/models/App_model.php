<?php defined('BASEPATH') OR exit('No direct script access allowed');

class App_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }


    public function login($email, $password)
    {
        $this->db->select('email', 'password');

        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $query = $this->db->get('student');

        return $query->row_array();
    }


    public function getLogin($email, $password, $status)
    {
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $this->db->where('status', $status);
        $query = $this->db->get('student');
        return $query->result();

    }

    public function teacher_Login($email, $password)
    {
        $this->db->select('email', 'password');

        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $query = $this->db->get('teacher');

        return $query->row_array();
    }

    public function get_teacher_Login($email, $password, $status)
    {
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $this->db->where('status', $status);
        $query = $this->db->get('teacher');
        return $query->result();

    }

    public function getTeacher()
    {
        $query = $this->db->get('teacher');
        return $query->result();
    }

    public function getStudent()
    {
        $query = $this->db->get('student');
        return $query->result();
    }

    public function settingTeacher()
    {
        $id = $this->input->post('id');
        $tname = $this->input->post('tname');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $phone = $this->input->post('phone');
        $office = $this->input->post('office');

        $this->db->set('tname', $tname);
        $this->db->set('email', $email);
        $this->db->set('password', $password);
        $this->db->set('phone', $phone);
        $this->db->set('office', $office);
        $this->db->where('id', $id);

        $result = $this->db->update('teacher');
        return $result;
    }

    public function getChat()
    {
        $this->db->select('c.sid ,s.sname ,c.tid,t.tname,c.pid ,c.chat,c.create_at ');
        $this->db->from('student s,teacher t,chat c');
        $this->db->where('c.sid = s.id and c.tid = t.id');


        $query = $this->db->get();
//        $query = $this->db->get('chat');
        return $query->result();

    }


    public function insertChat()
    {
        date_default_timezone_set('Asia/Taipei');
        $data = array(
            'sid' => $this->input->post('sid'),
            'tid' => $this->input->post('tid'),
            'chat' => $this->input->post('chat'),
            'pid' => $this->input->post('pid'),
            'create_at' => date('H:i a')
        );
        $result = $this->db->insert('chat', $data);
        return $result;
    }

    public function showChat()
    {
        $sql = "select chat,create_at,pid from chat";
        $query = $this->db->query($sql);
        $data = array();
        foreach ($query->result() as $row) {
            $data[] = $row->chat;
            $data[] = $row->create_at;

        }
        return $data;

    }

    public function startStatus()
    {
        $id = $_REQUEST['id'];
        $status = 1;
        $data = array('w_status' => $status);
        $this->db->where('id', $id);
        $result = $this->db->update('student', $data);
        return $result;
    }

    public function stopStatus()
    {
        $id = $_REQUEST['id'];
        $status = 0;
        $this->db->where('id', $id);
        $data = array(
            'w_status' => $status,
        );
        $result = $this->db->update('student', $data);
        return $result;
    }

    public function indexStop()
    {
        $id = $_REQUEST['id'];
        $status = 0;
        $this->db->where('id', $id);
        $data = array(
            'w_status' => $status,
        );
        $result = $this->db->update('student', $data);
        return $result;
    }


    public function insertLeaveMessage()
    {

        $data = array(
            'sid' => $this->input->post('sid'),
            'tid' => $this->input->post('tid'),
            'message' => $this->input->post('message')
        );
        $query = $this->db->insert('leave_message', $data);
        return $query->result();

    }

    public function showLeave()
    {
        $sql = 'SELECT l.t_del,l.id ,l.tid,l.sid,l.message,l.date,s.sname,t.tname FROM leave_message l,student s,teacher t 
                where l.sid = s.id and l.tid = t.id AND l.t_del=0 ORDER BY l.id';
        $query = $this->db->query($sql);
        return $query->result();
    }


    public function list_Leave($id)
    {

        $status = 1;
        $data = array('t_del' => $status);
        $this->db->where('id', $id);
        $result = $this->db->update('leave_message', $data);
        return $result;
    }

    public function showSLeave()
    {
        $sql = 'SELECT l.t_del,l.id ,l.tid,l.sid,l.message,l.date,s.sname,t.tname FROM leave_message l,student s,teacher t 
                where l.sid = s.id and l.tid = t.id AND l.s_del=0 ORDER BY l.id';
        $query = $this->db->query($sql);
        return $query->result();
    }


    public function list_SLeave($id)
    {
        $status = 1;
        $data = array('s_del' => $status);
        $this->db->where('id', $id);
        $result = $this->db->update('leave_message', $data);
        return $result;
    }


    public function loadEvents()
    {
        //load.php

        $connect = new PDO('mysql:host=localhost;dbname=duino', 'g8888', 'wsx71119');
        $connect->query('SET NAMES utf8');
        $data = array();

        $query = "SELECT * FROM events where send IS NULL and status is null and s_del=0 and t_del=0 ORDER BY id";

        $statement = $connect->prepare($query);

        $statement->execute();

        $result = $statement->fetchAll();

        foreach ($result as $row) {
            $data[] = array(
                'id' => $row["id"],
                'sid' => $row["sid"],
                "tid" => $row["tid"],
                'title' => $row["title"],
                'start' => $row["start_event"],
                'end' => $row["end_event"],
                's_del' => $row["s_del"],
                't_del' => $row["t_del"]
            );
        }

        return $data;

    }
	
	 public function insertEvents()
    {
        $connect = new PDO('mysql:host=localhost;dbname=duino', 'g8888', 'wsx71119');

        $connect->query('SET NAMES utf8');

        if (isset($_POST["title"])) {
            $query = "
                        INSERT INTO events 
                        (tid,title, start_event, end_event ,s_del, t_del) 
                        VALUES (:tid,:title, :start_event, :end_event, :s_del, :t_del)
                      ";
            $statement = $connect->prepare($query);
            $statement->execute(
                array(
                    'tid' => $_POST['tid'],
                    ':title' => $_POST['title'],
                    ':start_event' => $_POST['start'],
                    ':end_event' => $_POST['end'],
                    ':s_del' => $_POST['s_del'],
                    ':t_del' => $_POST['t_del']
                )
            );
        }
    }

    public function updateEvents()
    {

        $connect = new PDO('mysql:host=localhost;dbname=duino', 'g8888', 'wsx71119');

        if (isset($_POST["id"])) {
            $query = "
                       UPDATE events 
                       SET tid=:tid,title=:title, start_event=:start_event, end_event=:end_event,s_del=:s_del,t_del=:t_del 
                      WHERE id=:id
                      ";
            $statement = $connect->prepare($query);
            $statement->execute(
                array(
                    'tid' =>$_POST["tid"],
                    ':title' => $_POST['title'],
                    ':start_event' => $_POST['start'],
                    ':end_event' => $_POST['end'],
                    ':id' => $_POST['id'],
                    ':s_del' => $_POST['s_del'],
                    ':t_del' => $_POST['t_del']

                )
            );
        }
    }

    public function deleteEvents()
    {

        if (isset($_POST["id"])) {
            $connect = new PDO('mysql:host=localhost;dbname=duino', 'g8888', 'wsx71119');
            $query = "
                        UPDATE events 
                       SET s_del=:s_del, t_del=:t_del
                      WHERE id=:id
                      ";
            $statement = $connect->prepare($query);
            $statement->execute(
                array(
                    ':id' => $_POST['id'],
                    ':s_del' => $_POST['s_del'],
                    ':t_del' => $_POST['t_del']
                )
            );
        }


    }

    public function showSend()
    {
        $sql = 'SELECT s.sname,e.sid,e.title,e.s_del,e.t_del,e.status,e.send,e.start_event,e.id,e.end_event 
                FROM events e,student s where e.sid=s.id and e.title is null and e.t_del=0  ORDER BY e.id;';
        $query = $this->db->query($sql);
        return $query->result();
    }

//學生預約行程
    public function insertSend()
    {
        $connect = new PDO('mysql:host=localhost;dbname=duino', 'g8888', 'wsx71119');

        $connect->query('SET NAMES utf8');

        if (isset($_POST["send"])) {
            $query = "
                      INSERT INTO events 
                      (tid,sid,send, start_event, end_event ,s_del,t_del ) 
                      VALUES (:tid,:sid,:send, :start_event, :end_event, :s_del, :t_del)
                      ";
            $statement = $connect->prepare($query);
            $statement->execute(

                array(
                    'tid' => $_POST['tid'],
                    ':sid' => $_POST['sid'],
                    ':send' => $_POST['send'],
                    ':start_event' => $_POST['start'],
                    ':end_event' => $_POST['end'],
                    ':s_del' => $_POST['s_del'],
                    ':t_del' => $_POST['t_del']

                )
            );
        }
    }

//    public function loadSend()
//    {
//        //load.php
//
//        $connect = new PDO('mysql:host=localhost;dbname=duino', 'g8888', 'wsx71119');
//        $connect->query('SET NAMES utf8');
//        $data = array();
//
//        $query = "SELECT * FROM events where  ORDER BY id";
//
//        $statement = $connect->prepare($query);
//
//        $statement->execute();
//
//        $result = $statement->fetchAll();
//
//        foreach($result as $row)
//        {
//            $data[] = array(
//                'id'   => $row["id"],
//                'title'   => $row["title"],
//                'send'   => $row["send"],
//                'start'   => $row["start_event"],
//                'end'   => $row["end_event"]
//            );
//        }
//
//        echo json_encode($data);
//
//    }

//刪除預約
    public function list_send($id)
    {

        $status = 1;
        $data = array('t_del' => $status);
        $this->db->where('id', $id);
        $result = $this->db->update('events', $data);
        return $result;
    }

    public function list_mesend($id)
    {

        $status = 1;
        $data = array('s_del' => $status);
        $this->db->where('id', $id);
        $result = $this->db->update('events', $data);
        return $result;
    }

//確認學生預約
    public function check_send()
    {
        $id = $_REQUEST['id'];

        $send = '確認';

        $data = array('status' => $send);
        $this->db->where('id', $id);
        $result = $this->db->update('events', $data);
        return $result;
    }


    public function cancel_send()
    {
        $id = $_REQUEST['id'];

        $send = '取消';

        $data = array('status' => $send);
        $this->db->where('id', $id);
        $result = $this->db->update('events', $data);
        return $result;
    }


    public function meSend()
    {
        $sql = 'SELECT t.tname,e.sid,e.title,e.s_del,e.t_del,e.status,e.send,e.start_event,e.id,e.end_event 
                FROM events e,teacher t where e.sid=t.id and e.title is null and e.s_del=0  ORDER BY e.id;';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function deleteSend()
    {
        $id = $_REQUEST['id'];
        $this->db->where('id', $id);
        $result = $this->db->delete('events');
        return $result;
    }


    public function showrfid()
    {
        $sql = 'SELECT r.id,r.sid,r.tid,r.del,r.date,s.sname,t.tname FROM rfid r,student s,teacher t
                where r.sid=s.id and r.tid = t.id and  r.del=0 ORDER BY r.id';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function list_rfid($id)
    {
        $status = 1;
        $data = array('del' => $status);
        $this->db->where('id', $id);
        $result = $this->db->update('rfid', $data);
        return $result;
    }

}