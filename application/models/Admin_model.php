<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model
{


    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function login($email, $password)
    {
        $this->db->select('email', 'password');
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $query = $this->db->get('teacher');
        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function checkLogin($email, $password, $status)
    {
        $this->db->select('email', 'password', 'status');
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $this->db->where('status', $status);

        $query = $this->db->get('teacher');
        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }


    public function showTeacher()
    {
        $query = $this->db->get('teacher');
        return $query->result();
    }

    public function active()
    {
        $id = $_REQUEST['id'];
//        $action = $_REQUEST['status'];
//        if ($action == 1) {
//            $status = 0;
//        } else {
//            $status = 1;
//        }
        $status = 1;
        $data = array('status' => $status);
        $this->db->where('id', $id);
        $result = $this->db->update('teacher', $data);
        return $result;

    }

    public function inactive()
    {
        $id = $_REQUEST['id'];
//        $action = $_REQUEST['status'];
//        if ($action == 1) {
//            $status = 1;
//        } else {
//            $status = 0;
//        }
        $status = 0;
        $data = array('status' => $status);
        $this->db->where('id', $id);
        $result = $this->db->update('teacher', $data);
        return $result;
    }


    public function addTeacher()
    {
        date_default_timezone_set('Asia/Taipei');

        $data = array(
            'tname' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password'),
            'phone' => $this->input->post('phone'),
            'office' => $this->input->post('office'),
            'status' => $this->input->post('status'),
            'photo' => $this->input->post('photo'),
            'create_at' => date('Y-m-d H:i:s')
        );
        $result = $this->db->insert('teacher', $data);
        return $result;

    }

    public function updateTeacher()
    {
        $id = $this->input->post('id');
        $tname = $this->input->post('tname');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $phone = $this->input->post('phone');
        $office = $this->input->post('office');

        $this->db->set('tname', $tname);
        $this->db->set('email', $email);
        $this->db->set('password', $password);
        $this->db->set('phone', $phone);
        $this->db->set('office', $office);
        $this->db->where('id', $id);

        $result = $this->db->update('teacher');
        return $result;
    }

    public function deleteTeacher()
    {
        $id = $this->input->post('id');
        $this->db->where('id', $id);
        $result = $this->db->delete('teacher');
        return $result;
    }

    public function list_deleteT($id)
    {
//        $id = $this->input->post('id');
//        $this->db->where('id',$id);
//        $result = $this->db->delete('student');
//        return $result;
        $this->db->where('id', $id);
        $this->db->delete('teacher');
    }


    //Student

    public function showStudent()
    {
        $query = $this->db->get('student');
        return $query->result();
    }

    public function sActive()
    {
        $id = $_REQUEST['id'];
//        $action = $_REQUEST['status'];
//        if($action == 1){
//            $status = 0;
//        }else{
//            $status = 1;
//        }
        $status = 1;
        $data = array('status' => $status);
        $this->db->where('id', $id);
        $result = $this->db->update('student', $data);
        return $result;

    }

    public function sInactive()
    {
        $id = $_REQUEST['id'];
        $status = 0;
        $data = array('status' => $status);
        $this->db->where('id', $id);
        $result = $this->db->update('student', $data);
        return $result;
    }

    public function addStudent()
    {
        date_default_timezone_set('Asia/Taipei');
        $data = array(
            'sname' => $this->input->post('sname'),
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password'),
            'phone' => $this->input->post('phone'),
            'status' => $this->input->post('status'),
            'rfid' => $this->input->post('rfid'),
            'create_at' => date('Y-m-d H:i:s')
        );
        $result = $this->db->insert('student', $data);
        return $result;

    }

    public function updateStudent()
    {
        $id = $this->input->post('id');
        $name = $this->input->post('sname');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $phone = $this->input->post('phone');
        $status = $this->input->post('status');
        $rfid = $this->input->post('rfid');

        $this->db->set('sname', $name);
        $this->db->set('email', $email);
        $this->db->set('password', $password);
        $this->db->set('phone', $phone);
        $this->db->set('status', $status);
        $this->db->set('rfid',$rfid);
        $this->db->where('id', $id);

        $result = $this->db->update('student');
        return $result;
    }

    public function deleteStudent()
    {
        $id = $this->input->post('id');
        $this->db->where('id', $id);
        $result = $this->db->delete('student');
        return $result;
    }

    public function list_delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('student');
    }

    public function leaveMessage()
    {

        $sql = "select\n" .
            " l.sid,\n" .
            "s.id,\n" .
            "s.name,\n" .
            "l.message,\n" .
            "l.tid,\n" .
            "t.id,\n" .
            "t.name\n" .
            " from student s,\n" .
            "teacher t,\n" .
            "leave_message l\n" .
            " where l.sid\n" .
            " = s.id and t.id =\n" .
            " l.tid";
        $query = $this->db->query($sql);
        $data = array();
        foreach ($query->result() as $row) {
            $data[] = $row;
//            $data['t.name'] = $row;
        }
        return $data;
    }

    public function showChat()
    {
        $this->db->select('c.id,c.sid ,s.sname ,c.tid,t.tname,c.pid ,c.chat,c.create_at ');
        $this->db->from('student s,teacher t,chat c');
        $this->db->where('c.sid = s.id and c.tid = t.id');


        $query = $this->db->get();
        return $query->result();
    }

    public function list_delete_chat($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('chat');
    }

    public function showRfid(){
        $this->db->select('r.id,r.sid ,s.sname,r.tid ,t.tname,r.date ');
        $this->db->from('rfid r,student s,teacher t');
        $this->db->where('r.sid = s.id and r.tid = t.id');

        $query = $this->db->get();
        return $query->result();
    }

    public function list_delete_rfid($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('rfid');
    }

    public function showtitle(){
        $sql='SELECT e.id,e.tid,e.title,e.start_event,e.end_event,e.send ,e.status,t.tname 
              FROM events e,teacher t where  e.tid = t.id and e.send IS NULL and e.status is null ORDER BY e.id;';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function list_delete_title($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('events');
    }

    public function showsend(){
//        $sql='SELECT * FROM events where title is null  ORDER BY id';
//        $query = $this->db->query($sql);
//        return $query->result();

        $this->db->select('e.id,e.sid ,s.sname ,e.tid,t.tname ,e.send,e.start_event,e.end_event,e.status ');
        $this->db->from('student s,teacher t,events e');
        $this->db->where('e.sid = s.id and e.tid = t.id');


        $query = $this->db->get();
        return $query->result();
    }

    public function showleave(){
        $this->db->select('m.id,m.sid ,s.sname,m.tid ,t.tname,m.message,m.date ');
        $this->db->from('leave_message m,student s,teacher t');
        $this->db->where('m.sid = s.id and m.tid = t.id');

        $query = $this->db->get();
        return $query->result();
    }

    public function list_delete_leave($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('leave_message');
    }


}