-- --------------------------------------------------------
-- 主機:                           localhost
-- 伺服器版本:                        10.1.28-MariaDB - mariadb.org binary distribution
-- 伺服器操作系統:                      Win32
-- HeidiSQL 版本:                  9.5.0.5269
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 傾印 duino 的資料庫結構
CREATE DATABASE IF NOT EXISTS `duino` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `duino`;

-- 傾印  表格 duino.chat 結構
CREATE TABLE IF NOT EXISTS `chat` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `sid` int(20) DEFAULT NULL,
  `tid` int(20) DEFAULT NULL,
  `pid` varchar(50) DEFAULT NULL,
  `chat` varchar(100) DEFAULT NULL,
  `create_at` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_chat_student` (`sid`),
  KEY `FK_chat_teacher` (`tid`),
  CONSTRAINT `FK_chat_student` FOREIGN KEY (`sid`) REFERENCES `student` (`id`),
  CONSTRAINT `FK_chat_teacher` FOREIGN KEY (`tid`) REFERENCES `teacher` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- 正在傾印表格  duino.chat 的資料：~0 rows (大約)
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
INSERT INTO `chat` (`id`, `sid`, `tid`, `pid`, `chat`, `create_at`) VALUES
	(4, 1, 1, NULL, '策是聊天', '21:55 pm'),
	(5, 1, 1, '收到', NULL, '21:56 pm'),
	(6, 1, 2, NULL, 'yanlin test chat', '22:00 pm'),
	(8, 1, 2, '收到', NULL, '22:01 pm');
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;

-- 傾印  表格 duino.events 結構
CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) DEFAULT NULL,
  `tid` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `send` varchar(100) DEFAULT NULL,
  `s_del` int(20) DEFAULT NULL,
  `t_del` int(20) DEFAULT NULL,
  `start_event` datetime NOT NULL,
  `end_event` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_events_student` (`sid`),
  KEY `FK_events_teacher` (`tid`),
  CONSTRAINT `FK_events_student` FOREIGN KEY (`sid`) REFERENCES `student` (`id`),
  CONSTRAINT `FK_events_teacher` FOREIGN KEY (`tid`) REFERENCES `teacher` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- 正在傾印表格  duino.events 的資料：~0 rows (大約)
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` (`id`, `sid`, `tid`, `title`, `status`, `send`, `s_del`, `t_del`, `start_event`, `end_event`) VALUES
	(4, 1, 1, NULL, '確認', 'test calendar', 0, 0, '2018-06-30 00:00:00', '2018-07-01 00:00:00'),
	(5, NULL, 1, 'teacher', NULL, NULL, 0, 0, '2018-06-30 00:00:00', '2018-07-01 00:00:00'),
	(6, NULL, 2, 'yanlin', NULL, NULL, 0, 0, '2018-06-29 00:00:00', '2018-06-30 00:00:00'),
	(7, 1, 2, NULL, '取消', 'yanlin go', 0, 0, '2018-06-29 00:00:00', '2018-06-30 00:00:00');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;

-- 傾印  表格 duino.leave_message 結構
CREATE TABLE IF NOT EXISTS `leave_message` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `sid` int(20) DEFAULT NULL,
  `tid` int(20) DEFAULT NULL,
  `message` varchar(100) DEFAULT NULL,
  `s_del` int(20) NOT NULL DEFAULT '0',
  `t_del` int(20) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_leave_message_student` (`sid`),
  KEY `FK_leave_message_teacher` (`tid`),
  CONSTRAINT `FK_leave_message_student` FOREIGN KEY (`sid`) REFERENCES `student` (`id`),
  CONSTRAINT `FK_leave_message_teacher` FOREIGN KEY (`tid`) REFERENCES `teacher` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- 正在傾印表格  duino.leave_message 的資料：~0 rows (大約)
/*!40000 ALTER TABLE `leave_message` DISABLE KEYS */;
INSERT INTO `leave_message` (`id`, `sid`, `tid`, `message`, `s_del`, `t_del`, `date`) VALUES
	(3, 1, 1, 'test leave\n', 0, 0, '2018-06-30 21:55:35'),
	(4, 1, 2, 'yanlin test leave', 0, 0, '2018-06-30 22:00:25');
/*!40000 ALTER TABLE `leave_message` ENABLE KEYS */;

-- 傾印  表格 duino.rfid 結構
CREATE TABLE IF NOT EXISTS `rfid` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `sid` int(20) DEFAULT NULL,
  `tid` int(20) DEFAULT NULL,
  `del` int(20) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_rfid_student` (`sid`),
  KEY `FK_rfid_teacher` (`tid`),
  CONSTRAINT `FK_rfid_student` FOREIGN KEY (`sid`) REFERENCES `student` (`id`),
  CONSTRAINT `FK_rfid_teacher` FOREIGN KEY (`tid`) REFERENCES `teacher` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 正在傾印表格  duino.rfid 的資料：~0 rows (大約)
/*!40000 ALTER TABLE `rfid` DISABLE KEYS */;
INSERT INTO `rfid` (`id`, `sid`, `tid`, `del`, `date`) VALUES
	(1, 1, 1, 0, '2018-06-30 22:03:06'),
	(2, 2, 2, 0, '2018-06-30 22:05:26');
/*!40000 ALTER TABLE `rfid` ENABLE KEYS */;

-- 傾印  表格 duino.student 結構
CREATE TABLE IF NOT EXISTS `student` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `status` int(1) DEFAULT NULL,
  `w_status` int(1) DEFAULT NULL,
  `sname` varchar(50) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `rfid` varchar(50) NOT NULL,
  `update` datetime NOT NULL,
  `create_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 正在傾印表格  duino.student 的資料：~0 rows (大約)
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` (`id`, `status`, `w_status`, `sname`, `email`, `password`, `phone`, `rfid`, `update`, `create_at`) VALUES
	(1, 1, 2, 'student', 'student@test.com', 'student', NULL, 'TEST456', '2018-06-30 21:33:29', '2018-06-30 21:33:30'),
	(2, 1, NULL, 'test', 'test@test.com', 'test', '', 'ABC123', '0000-00-00 00:00:00', '2018-06-30 22:04:47');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;

-- 傾印  表格 duino.teacher 結構
CREATE TABLE IF NOT EXISTS `teacher` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `status` int(1) DEFAULT NULL,
  `tname` varchar(50) DEFAULT NULL,
  `photo` varchar(150) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `office` varchar(50) DEFAULT NULL,
  `update` datetime NOT NULL,
  `create_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 正在傾印表格  duino.teacher 的資料：~0 rows (大約)
/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
INSERT INTO `teacher` (`id`, `status`, `tname`, `photo`, `email`, `password`, `phone`, `office`, `update`, `create_at`) VALUES
	(1, 1, 'admin', 'gui.png', 'admin@admin.com', 'admin', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 1, 'yanlin', 'dong.png', 'yanlin@gmail.com', 'yanlin', '', '', '0000-00-00 00:00:00', '2018-06-30 21:56:56');
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;

-- 傾印  表格 duino.webduino 結構
CREATE TABLE IF NOT EXISTS `webduino` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `tid` int(20) DEFAULT NULL,
  `office` varchar(50) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `led` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_webduino_teacher` (`tid`),
  CONSTRAINT `FK_webduino_teacher` FOREIGN KEY (`tid`) REFERENCES `teacher` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 正在傾印表格  duino.webduino 的資料：~2 rows (大約)
/*!40000 ALTER TABLE `webduino` DISABLE KEYS */;
INSERT INTO `webduino` (`id`, `tid`, `office`, `status`, `name`, `led`) VALUES
	(1, 1, 'F000', 1, 'mbN3p', 0),
	(2, 1, 'F000', 0, 'n4kEE', 1);
/*!40000 ALTER TABLE `webduino` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
